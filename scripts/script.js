class Card {
    constructor(postId, title, text, userName, userEmail) {
        this.postId = postId;
        this.title = title;
        this.text = text;
        this.userName = userName;
        this.userEmail = userEmail;
    }

    render() {
        const cardElement = document.createElement('div');
        cardElement.classList.add('card');
        cardElement.innerHTML = `
          <h2>${this.title}</h2>
          <p>${this.text}</p>
          <p>Posted by: ${this.userName} (${this.userEmail})</p>
          <button class="delete-btn" data-post-id="${this.postId}">Delete</button>
        `;
        return cardElement;
    }
}

async function fetchData(url) {
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error('Failed to fetch data');
        }
        return await response.json();
    } catch (error) {
        console.error('Error fetching data:', error);
    }
}

async function deletePost(postId) {
    try {
        const response = await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
            method: 'DELETE'
        });
        if (!response.ok) {
            throw new Error('Failed to delete post');
        }
        return true;
    } catch (error) {
        console.error('Error deleting post:', error);
        return false;
    }
}

async function initialize() {
    const users = await fetchData('https://ajax.test-danit.com/api/json/users');
    const posts = await fetchData('https://ajax.test-danit.com/api/json/posts');

    const newsFeed = document.getElementById('news-feed');
    posts.forEach(post => {
        const user = users.find(user => user.id === post.userId);
        if (user) {
            const card = new Card(post.id, post.title, post.body, user.name, user.email);
            const cardElement = card.render();
            const deleteBtn = cardElement.querySelector('.delete-btn');
            deleteBtn.addEventListener('click', async () => {
                const success = await deletePost(post.id);
                if (success) {
                    cardElement.remove();
                }
            });
            newsFeed.appendChild(cardElement);
        }
    });
}

initialize();